const averageSort = (data) => {
  const sorted = data.sort((a, b) => {
    return +b.value - +a.value
  })

  const max = sorted[0]
  const min = sorted[sorted.length - 1]

  const maxPercent = 100
  const minPercent = 28
  const elastic = 100 / (maxPercent - minPercent)

  let result = []

  sorted.forEach(item => {
    const minused = item.value - min.value
    const elasticPercent = minused / (max.value - min.value) * maxPercent

    result.push({...item, percent: elasticPercent / elastic + minPercent})
  });


  return result
}

data = [
  {
    name: 'Ибрагимов Иван',
    title: 'Оптовый',
    value: 10000,
  },
  {
    name: 'Подгородецкий Владимир',
    title: 'Продажный',
    value: 2999,
  },
  {
    name: 'Кржемелик Вахмурка',
    title: 'Лоховской',
    value: 3000,
  },
  {
    name: 'Кремчик',
    title: 'Программисты',
    value: 4000,
  },
  {
    name: 'Ибрагимов Иван',
    title: 'Оптовый',
    value: 5000,
  },
  {
    name: 'Подгородецкий Владимир',
    title: 'Продажный',
    value: 6000,
  },
  {
    name: 'Кржемелик Вахмурка',
    title: 'Лоховской',
    value: 7000,
  },
  {
    name: 'Кремчик',
    title: 'Программисты',
    value: 8000,
  },
]
//топ ПРОДАЖ (месяц)
const colors1 = ['#58B7DE', '#5895DE', '#5B65DE', '#735BDE', '#A15BDE', '#C85BDE', '#DE5BD0', '#DE5BAA']

//топ ПРОДАЖ акционных товаров (месяц)
const colors2 = ['#DC8C67', '#DC6967', '#DC6788', '#DC67AB', '#C767DC', '#A367DC', '#8067DC', '#6771DC']

//план продаж (месяц) Лучших
const colors3 = ['#3992E0', '#E97E39', '#009786']

//план продаж (месяц) Худших
const colors4 = ['#735BDE', '#F156E2', '#A668E5']

//количество работающих клиентов Лучших
const colors5 = ['#1AACC0', '#D355E8', '#397CE0']

//количество работающих клиентов Худших
const colors6 = ['#13B685', '#E3683F', '#F29E3C']

//объем продаж (месяц) Лучших
const colors7 = ['#EEA717', '#F16375', '#907CDE']

//объем продаж (месяц) Худших
const colors8 = ['#FF968A', '#BD70D8', '#BD70D8']

//продажа акционных товаров (месяц) Лучших
const colors9 = ['#1797E9', '#00BDCC', '#EA9D26']

//продажа акционных товаров (месяц) Худших
const colors10 = ['#16B66C', '#9DC11F', '#F59855']

//размер дебиторской задолженности (месяц) Лучших
const colors11 = ['#907CDE', '#907CDE', '#907CDE']

//размер дебиторской задолженности (месяц) Худших
const colors12 = ['#F49855', '#F49855', '#F49855']

//размер дебиторской задолженности (месяц) Лучших
const colors13 = ['#FF8966', '#21A4DD', '#21A4DD']

//размер дебиторской задолженности (месяц) Худших
const colors14 = ['#CD70F4', '#D1608B', '#907CDE']


document.addEventListener("DOMContentLoaded", function () {

  const sorted = averageSort(data);

  // console.log('in', JSON.stringify(data))
  // console.log('out', JSON.stringify(sorted))

  const items = document.getElementById('items')
  console.log(items)

  sorted.forEach((item, index) => {
    items.innerHTML += `
    <div class="item">
        <div class="item__header">
        <div class="item__employee item-employee">
              <div class="item-employee__photo">
                <img src="./img/photo.jpg" alt="Сотрудник">
              </div>
              <div class="item-employee__name">
                Подгородецкий
                <br>
                Владислав
              </div>
            </div>
          <div class="item__title">
            ${item.title}
          </div>
        </div>

        <div class="item__body">
          <div class="item-grafik" style=" height: ${item.percent}%; background-color: ${colors1[index]}">
            <div class="item__value">${item.value} ₽ </div>
          </div>
        </div>
    </div>
  `
  })

});

